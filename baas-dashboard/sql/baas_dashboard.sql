# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.16-MariaDB)
# Database: baas_dashboard
# Generation Time: 2017-11-29 14:32:17 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE DATABASE `baas2_dashboard` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `baas2_dashboard`;

# Dump of table action
# ------------------------------------------------------------

DROP TABLE IF EXISTS `action`;

CREATE TABLE `action` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `action_name` text COMMENT '权限名称',
  `action` varchar(32) DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `action_foreign` (`action`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table baas_plugins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `baas_plugins`;

CREATE TABLE `baas_plugins` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `baas_id` int(11) NOT NULL DEFAULT '0' COMMENT '应用id',
  `addons_id` int(11) NOT NULL DEFAULT '0' COMMENT '插件id',
  `started_at` timestamp NULL DEFAULT NULL COMMENT '开始时间',
  `ended_at` timestamp NULL DEFAULT NULL COMMENT '结束时间',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `baas_plugins` WRITE;
/*!40000 ALTER TABLE `baas_plugins` DISABLE KEYS */;

INSERT INTO `baas_plugins` (`id`, `baas_id`, `addons_id`, `started_at`, `ended_at`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,1,'2017-11-15 09:51:22','2017-11-22 09:51:22','2017-11-15 09:51:22','2017-11-15 09:51:22',NULL);

/*!40000 ALTER TABLE `baas_plugins` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table company
# ------------------------------------------------------------

DROP TABLE IF EXISTS `company`;

CREATE TABLE `company` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `company_name` text COMMENT '企业名称',
  `credit_num` text COMMENT '企业信用代码',
  `license_id` int(11) NOT NULL DEFAULT '0' COMMENT '执照图片id',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;

INSERT INTO `company` (`id`, `user_id`, `company_name`, `credit_num`, `license_id`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,'杰威尔','81410408MA3X7QH85P',3,'2017-11-27 14:21:11','2017-11-27 14:29:46',NULL),
	(2,3,'青否科技','112351123511235111',35,'2017-11-28 10:57:25','2017-11-28 18:39:31',NULL);

/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table notice
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notice`;

CREATE TABLE `notice` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` text COMMENT '消息标题',
  `content` text COMMENT '消息内容',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `notice` WRITE;
/*!40000 ALTER TABLE `notice` DISABLE KEYS */;

INSERT INTO `notice` (`id`, `title`, `content`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'中奖提醒','恭喜你获得奥利奥小饼干一个！',NULL,NULL,NULL),
	(2,'中奖提醒','恭喜你获得200平别墅宣传页一张！',NULL,NULL,NULL);

/*!40000 ALTER TABLE `notice` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plugins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plugins`;

CREATE TABLE `plugins` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL DEFAULT '0' COMMENT '分类id',
  `name` text COMMENT '插件名称',
  `price` int(11) NOT NULL DEFAULT '0' COMMENT '服务价格',
  `cycle` int(11) NOT NULL DEFAULT '0' COMMENT '服务周期',
  `remark` text COMMENT '插件备注',
  `file_id` int(11) NOT NULL DEFAULT '0' COMMENT '图片id',
  `detail` text COMMENT '应用详情',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plugins` WRITE;
/*!40000 ALTER TABLE `plugins` DISABLE KEYS */;

INSERT INTO `plugins` (`id`, `menu_id`, `name`, `price`, `cycle`, `remark`, `file_id`, `detail`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,'验证码',2999,12,'最牛逼的短信',8,'<p style=\"width:986px\"><img src=\"https://api.qingful.com/uploads/2017-09-25/8cff44c7-12c1-4fdb-80ec-4d92249cf6ca.jpg\" alt=\"智慧餐饮专题副本_01\" style=\"max-width:100%;\"><img src=\"https://api.qingful.com/uploads/2017-09-25/f046abe9-64bb-4859-871b-5040b1b12502.jpg\" alt=\"智慧餐饮专题副本_02\" style=\"max-width: 100%;\"><img src=\"https://api.qingful.com/uploads/2017-09-25/f8a7c842-f5dc-4817-830c-ada0add7b1e0.jpg\" alt=\"智慧餐饮专题副本_03\" style=\"max-width: 100%;\"><img src=\"https://api.qingful.com/uploads/2017-09-25/22721a43-8f56-49df-adbf-ebd6a8151ef5.jpg\" alt=\"智慧餐饮专题副本_05\" style=\"max-width: 100%;\"><img src=\"https://api.qingful.com/uploads/2017-09-25/8cd2e324-f788-47cd-8363-d0872c2aa707.jpg\" alt=\"智慧餐饮专题副本_06\" style=\"max-width: 100%;\"><img src=\"https://api.qingful.com/uploads/2017-09-25/e2cc501b-d299-41c3-a239-4754c02a88e9.jpg\" alt=\"智慧餐饮专题副本_07\" style=\"max-width: 100%;\"><br></p><p><br></p>\n',NULL,NULL,NULL);

/*!40000 ALTER TABLE `plugins` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plugins_menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plugins_menu`;

CREATE TABLE `plugins_menu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plugins_menu` WRITE;
/*!40000 ALTER TABLE `plugins_menu` DISABLE KEYS */;

INSERT INTO `plugins_menu` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'营销',NULL,NULL,NULL);

/*!40000 ALTER TABLE `plugins_menu` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plugins_poster
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plugins_poster`;

CREATE TABLE `plugins_poster` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `plugins_id` int(11) NOT NULL DEFAULT '0' COMMENT '插件id',
  `file_id` int(11) NOT NULL DEFAULT '0' COMMENT '插件图片id',
  `created_at` timestamp NULL DEFAULT NULL,
  `upadted_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `plugins_poster` WRITE;
/*!40000 ALTER TABLE `plugins_poster` DISABLE KEYS */;

INSERT INTO `plugins_poster` (`id`, `plugins_id`, `file_id`, `created_at`, `upadted_at`, `deleted_at`)
VALUES
	(1,1,7,NULL,NULL,NULL);

/*!40000 ALTER TABLE `plugins_poster` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COMMENT '角色名称',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table role_action
# ------------------------------------------------------------

DROP TABLE IF EXISTS `role_action`;

CREATE TABLE `role_action` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) unsigned DEFAULT '0' COMMENT '角色id',
  `action_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '权限id',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_foreign` (`role_id`),
  KEY `role_action-action` (`action_id`),
  CONSTRAINT `role_action-action` FOREIGN KEY (`action_id`) REFERENCES `action` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `role_action-role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table trade
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trade`;

CREATE TABLE `trade` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `tradeid` text,
  `money` float NOT NULL DEFAULT '0',
  `payment` text,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0: 未支付，1: 支付成功，-1: 退款成功',
  `notify_url` text,
  `notify_data` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `trade` WRITE;
/*!40000 ALTER TABLE `trade` DISABLE KEYS */;

INSERT INTO `trade` (`id`, `user_id`, `tradeid`, `money`, `payment`, `status`, `notify_url`, `notify_data`, `created_at`, `updated_at`)
VALUES
	(1,4,'1711271351412',2,'支付宝',0,'/home/public/ump_pay_success','{\"user_id\":4}','2017-11-27 13:51:41','2017-11-27 13:51:41'),
	(2,1,'1711282007480',1,'支付宝',0,'/home/public/ump_pay_success','{\"user_id\":1}','2017-11-28 20:07:48','2017-11-28 20:07:48'),
	(3,6,'1711290925350',1,'支付宝',0,'/home/public/ump_pay_success','{\"user_id\":6}','2017-11-29 09:25:35','2017-11-29 09:25:35'),
	(4,3,'1711291109067',12,'支付宝',0,'/home/public/ump_pay_success','{\"user_id\":3}','2017-11-29 11:09:06','2017-11-29 11:09:06'),
	(5,3,'1711291130056',1,'支付宝',0,'/home/public/ump_pay_success','{\"user_id\":3}','2017-11-29 11:30:05','2017-11-29 11:30:05'),
	(6,5,'1711291424548',0.01,'支付宝',0,'/home/public/ump_pay_success','{\"user_id\":5}','2017-11-29 14:24:54','2017-11-29 14:24:54'),
	(7,3,'1711291620372',100,'支付宝',0,'/home/public/ump_pay_success','{\"user_id\":3}','2017-11-29 16:20:37','2017-11-29 16:20:37'),
	(8,6,'1711291622337',0.01,'支付宝',0,'/home/public/ump_pay_success','{\"user_id\":6}','2017-11-29 16:22:33','2017-11-29 16:22:33');

/*!40000 ALTER TABLE `trade` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `phone` text COMMENT '手机号',
  `password` text COMMENT '登录密码',
  `password_org` text COMMENT '未加密登录密码',
  `lastip` text COMMENT '最后登录ip',
  `money` float(10,2) NOT NULL DEFAULT '0.00' COMMENT '账户余额',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '用户状态',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `phone`, `password`, `password_org`, `lastip`, `money`, `status`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'18538553590','c4ca4238a0b923820dcc509a6f75849b','1',NULL,90.00,1,'2017-11-27 10:16:05','2017-11-29 21:52:55',NULL),
	(2,'18538069497','e451ff39540c8af467094d93b9922975','wulei199231024',NULL,0.00,1,'2017-11-27 10:16:16','2017-11-27 10:16:16',NULL),
	(3,'15738361451','c4ca4238a0b923820dcc509a6f75849b','1',NULL,190.00,1,'2017-11-27 10:18:14','2017-11-29 22:15:50',NULL),
	(4,'17638188752','c4ca4238a0b923820dcc509a6f75849b','1',NULL,0.00,1,'2017-11-27 13:50:36','2017-11-27 13:50:36',NULL),
	(5,'13253522080','13768f3411147a5d215843f372b4b016','Dxs56336',NULL,0.00,1,'2017-11-28 18:45:08','2017-11-28 18:45:08',NULL),
	(6,'18338767481','c03c9d29c45bc2798fe5c9eec2f071c3','zhangpeng123',NULL,0.00,1,'2017-11-28 19:11:22','2017-11-28 19:11:22',NULL),
	(7,'18790287977','c4ca4238a0b923820dcc509a6f75849b','1',NULL,0.00,1,'2017-11-29 11:14:55','2017-11-29 11:14:55',NULL),
	(8,'17516209689','c4ca4238a0b923820dcc509a6f75849b','1',NULL,0.00,1,'2017-11-29 13:41:12','2017-11-29 13:41:12',NULL);

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_baas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_baas`;

CREATE TABLE `user_baas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `baas_id` int(11) NOT NULL DEFAULT '0' COMMENT '应用id',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user_baas` WRITE;
/*!40000 ALTER TABLE `user_baas` DISABLE KEYS */;

INSERT INTO `user_baas` (`id`, `user_id`, `baas_id`, `created_at`, `updated_at`)
VALUES
	(1,1,1,'2017-11-27 10:27:35','2017-11-27 10:27:35'),
	(2,2,2,'2017-11-27 10:28:16','2017-11-27 10:28:16'),
	(3,3,3,'2017-11-27 10:29:25','2017-11-27 10:29:25'),
	(4,3,4,'2017-11-27 10:31:25','2017-11-27 10:31:25'),
	(5,1,5,'2017-11-27 11:01:24','2017-11-27 11:01:24'),
	(6,2,6,'2017-11-27 11:40:52','2017-11-27 11:40:52'),
	(7,3,7,'2017-11-27 11:50:03','2017-11-27 11:50:03'),
	(8,1,8,'2017-11-27 12:00:04','2017-11-27 12:00:04'),
	(9,4,0,'2017-11-27 13:50:36','2017-11-27 13:50:36'),
	(10,4,9,'2017-11-27 13:52:37','2017-11-27 13:52:37'),
	(11,2,10,'2017-11-27 15:58:26','2017-11-27 15:58:26'),
	(12,5,0,'2017-11-28 18:45:08','2017-11-28 18:45:08'),
	(13,5,11,'2017-11-28 18:46:23','2017-11-28 18:46:23'),
	(14,6,0,'2017-11-28 19:11:22','2017-11-28 19:11:22'),
	(15,6,12,'2017-11-28 19:11:46','2017-11-28 19:11:46'),
	(16,6,13,'2017-11-28 19:11:56','2017-11-28 19:11:56'),
	(17,6,14,'2017-11-28 19:12:14','2017-11-28 19:12:14'),
	(18,6,15,'2017-11-28 19:16:26','2017-11-28 19:16:26'),
	(19,3,16,'2017-11-28 19:41:13','2017-11-28 19:41:13'),
	(20,3,17,'2017-11-28 19:42:19','2017-11-28 19:42:19'),
	(21,3,18,'2017-11-28 19:42:22','2017-11-28 19:42:22'),
	(22,3,19,'2017-11-28 19:42:26','2017-11-28 19:42:26'),
	(23,3,20,'2017-11-28 19:42:29','2017-11-28 19:42:29'),
	(24,3,21,'2017-11-28 19:43:03','2017-11-28 19:43:03'),
	(25,3,22,'2017-11-28 19:43:06','2017-11-28 19:43:06'),
	(26,3,23,'2017-11-28 19:43:10','2017-11-28 19:43:10'),
	(27,3,24,'2017-11-28 19:43:14','2017-11-28 19:43:14'),
	(28,3,25,'2017-11-28 19:43:21','2017-11-28 19:43:21'),
	(29,3,26,'2017-11-28 19:43:29','2017-11-28 19:43:29'),
	(30,3,27,'2017-11-28 19:43:38','2017-11-28 19:43:38'),
	(31,3,28,'2017-11-28 19:43:43','2017-11-28 19:43:43'),
	(32,3,29,'2017-11-28 19:43:50','2017-11-28 19:43:50'),
	(33,3,30,'2017-11-28 20:08:26','2017-11-28 20:08:26'),
	(34,5,31,'2017-11-29 10:46:17','2017-11-29 10:46:17'),
	(35,5,32,'2017-11-29 10:49:33','2017-11-29 10:49:33'),
	(36,7,0,'2017-11-29 11:14:55','2017-11-29 11:14:55'),
	(37,8,0,'2017-11-29 13:41:12','2017-11-29 13:41:12'),
	(38,8,33,'2017-11-29 14:05:25','2017-11-29 14:05:25'),
	(39,8,34,'2017-11-29 14:25:12','2017-11-29 14:25:12'),
	(40,7,35,'2017-11-29 16:52:14','2017-11-29 16:52:14'),
	(42,3,37,'2017-11-29 21:33:08','2017-11-29 21:33:08'),
	(43,1,38,'2017-11-29 22:29:04','2017-11-29 22:29:04');

/*!40000 ALTER TABLE `user_baas` ENABLE KEYS */;
UNLOCK TABLES;

DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `sms_add` BEFORE INSERT ON `user_baas` FOR EACH ROW BEGIN
insert into baas_plugin.sms(baas_id,created_at) values(new.baas_id,new.created_at);
END */;;
/*!50003 SET SESSION SQL_MODE="NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `log_count_add` AFTER INSERT ON `user_baas` FOR EACH ROW BEGIN
insert into baas_analysis.statistics(baas_id,flow,storage,created_at) values(new.baas_id,'0B','0B',new.created_at);
END */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# Dump of table user_image
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_image`;

CREATE TABLE `user_image` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `name` text,
  `ext` text,
  `type` text,
  `savename` text,
  `savepath` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user_image` WRITE;
/*!40000 ALTER TABLE `user_image` DISABLE KEYS */;

INSERT INTO `user_image` (`id`, `user_id`, `name`, `ext`, `type`, `savename`, `savepath`, `created_at`, `updated_at`)
VALUES
	(1,1,'avatar5.png','png','image/png','41e5b2c0-d326-11e7-b4f1-5fad8fe1e5d9.png','2017-11-27','2017-11-27 11:51:32','2017-11-27 11:51:32'),
	(2,1,'avatar5.png','png','image/png','76abad70-d326-11e7-b4df-374a0fa198f4.png','2017-11-27','2017-11-27 11:53:01','2017-11-27 11:53:01'),
	(3,1,'avatar5.png','png','image/png','7c2b9b20-d326-11e7-b4df-374a0fa198f4.png','2017-11-27','2017-11-27 11:53:10','2017-11-27 11:53:10'),
	(4,2,'正方形.jpg','jpg','image/jpeg','934c7950-d326-11e7-b4df-374a0fa198f4.jpg','2017-11-27','2017-11-27 11:53:49','2017-11-27 11:53:49'),
	(5,1,'user1-128x128.jpg','jpg','image/jpeg','5f9ecea0-d335-11e7-9dd4-9984d110aaec.jpg','2017-11-27','2017-11-27 13:39:44','2017-11-27 13:39:44'),
	(6,1,'avatar04.png','png','image/png','34ebf9d0-d33f-11e7-9219-bb983153fd94.png','2017-11-27','2017-11-27 14:50:08','2017-11-27 14:50:08'),
	(7,1,'avatar04.png','png','image/png','3966ac80-d33f-11e7-9219-bb983153fd94.png','2017-11-27','2017-11-27 14:50:15','2017-11-27 14:50:15'),
	(8,3,'2.png','png','image/png','bc9f9fd0-d358-11e7-a59e-7966ad6518ef.png','2017-11-27','2017-11-27 17:52:53','2017-11-27 17:52:53'),
	(9,3,'2.png','png','image/png','f764cc80-d367-11e7-a32a-21942870bdc2.png','2017-11-27','2017-11-27 19:41:54','2017-11-27 19:41:54'),
	(10,3,'563c52bb4b7eb.jpg','jpg','image/jpeg','fd4bb960-d367-11e7-a32a-21942870bdc2.jpg','2017-11-27','2017-11-27 19:42:04','2017-11-27 19:42:04'),
	(11,3,'563c4f4339b2d.jpg','jpg','image/jpeg','24adf810-d368-11e7-a32a-21942870bdc2.jpg','2017-11-27','2017-11-27 19:43:10','2017-11-27 19:43:10'),
	(12,3,'563c52bb4bced.jpg','jpg','image/jpeg','44c1f1b0-d368-11e7-a32a-21942870bdc2.jpg','2017-11-27','2017-11-27 19:44:04','2017-11-27 19:44:04'),
	(13,3,'563c52bb4b7eb.jpg','jpg','image/jpeg','22c78f60-d369-11e7-a32a-21942870bdc2.jpg','2017-11-27','2017-11-27 19:50:16','2017-11-27 19:50:16'),
	(14,3,'563c4f4337c28.jpg','jpg','image/jpeg','2909e2b0-d369-11e7-a32a-21942870bdc2.jpg','2017-11-27','2017-11-27 19:50:27','2017-11-27 19:50:27'),
	(15,3,'563c52bb4b7eb.jpg','jpg','image/jpeg','3bd60cc0-d369-11e7-a32a-21942870bdc2.jpg','2017-11-27','2017-11-27 19:50:58','2017-11-27 19:50:58'),
	(16,3,'563c52bb4bced.jpg','jpg','image/jpeg','74c9ab90-d369-11e7-a32a-21942870bdc2.jpg','2017-11-27','2017-11-27 19:52:34','2017-11-27 19:52:34'),
	(17,3,'563c52bb4bced.jpg','jpg','image/jpeg','8b0f9310-d369-11e7-a32a-21942870bdc2.jpg','2017-11-27','2017-11-27 19:53:11','2017-11-27 19:53:11'),
	(18,3,'563c4f4337c28.jpg','jpg','image/jpeg','9c36bfb0-d369-11e7-a32a-21942870bdc2.jpg','2017-11-27','2017-11-27 19:53:40','2017-11-27 19:53:40'),
	(19,3,'563c52bb4b7eb.jpg','jpg','image/jpeg','e7299ba0-d369-11e7-a32a-21942870bdc2.jpg','2017-11-27','2017-11-27 19:55:46','2017-11-27 19:55:46'),
	(20,3,'56e7c2db0077e.jpg','jpg','image/jpeg','7e92b530-d3d8-11e7-8627-cf2c994a54cb.jpg','2017-11-28','2017-11-28 09:07:24','2017-11-28 09:07:24'),
	(21,3,'56e7c380de450.jpg','jpg','image/jpeg','b1388f00-d3d8-11e7-8627-cf2c994a54cb.jpg','2017-11-28','2017-11-28 09:08:49','2017-11-28 09:08:49'),
	(22,3,'563c52bb4bced.jpg','jpg','image/jpeg','cf6c2fe0-d3d8-11e7-8627-cf2c994a54cb.jpg','2017-11-28','2017-11-28 09:09:40','2017-11-28 09:09:40'),
	(23,3,'563c52bb4bced.jpg','jpg','image/jpeg','f2331480-d3d8-11e7-8627-cf2c994a54cb.jpg','2017-11-28','2017-11-28 09:10:38','2017-11-28 09:10:38'),
	(24,3,'563c52bb4bced.jpg','jpg','image/jpeg','5707c360-d3d9-11e7-8627-cf2c994a54cb.jpg','2017-11-28','2017-11-28 09:13:27','2017-11-28 09:13:27'),
	(25,3,'56e7c2db0077e.jpg','jpg','image/jpeg','7f7b4b00-d3d9-11e7-8627-cf2c994a54cb.jpg','2017-11-28','2017-11-28 09:14:35','2017-11-28 09:14:35'),
	(26,3,'563c52bb4bced.jpg','jpg','image/jpeg','912d2b70-d3d9-11e7-8627-cf2c994a54cb.jpg','2017-11-28','2017-11-28 09:15:05','2017-11-28 09:15:05'),
	(27,3,'563c52bb4bced.jpg','jpg','image/jpeg','a200e0e0-d3d9-11e7-8627-cf2c994a54cb.jpg','2017-11-28','2017-11-28 09:15:33','2017-11-28 09:15:33'),
	(28,3,'563c52bb4b7eb.jpg','jpg','image/jpeg','b9616020-d3d9-11e7-8627-cf2c994a54cb.jpg','2017-11-28','2017-11-28 09:16:12','2017-11-28 09:16:12'),
	(29,3,'563c52bb4b7eb.jpg','jpg','image/jpeg','d76fa940-d3da-11e7-8f36-4ded213f04fa.jpg','2017-11-28','2017-11-28 09:24:12','2017-11-28 09:24:12'),
	(30,3,'563c52bb4b7eb.jpg','jpg','image/jpeg','f8652940-d3da-11e7-8f36-4ded213f04fa.jpg','2017-11-28','2017-11-28 09:25:08','2017-11-28 09:25:08'),
	(31,3,'563c52bb4bced.jpg','jpg','image/jpeg','0ed86250-d3db-11e7-8f36-4ded213f04fa.jpg','2017-11-28','2017-11-28 09:25:45','2017-11-28 09:25:45'),
	(32,3,'563c52bb4b7eb.jpg','jpg','image/jpeg','3c166a50-d3db-11e7-be41-ebe271bd896c.jpg','2017-11-28','2017-11-28 09:27:01','2017-11-28 09:27:01'),
	(33,3,'563c52adac85f.jpg','jpg','image/jpeg','4f2eff50-d3de-11e7-ad2a-098d6e23ffb5.jpg','2017-11-28','2017-11-28 09:49:02','2017-11-28 09:49:02'),
	(34,3,'563c52bb4b7eb.jpg','jpg','image/jpeg','cfe24c70-d3e7-11e7-8709-ddac51d2acf5.jpg','2017-11-28','2017-11-28 10:57:03','2017-11-28 10:57:03'),
	(35,3,'563c52adac85f.jpg','jpg','image/jpeg','01aeb630-d3e8-11e7-8709-ddac51d2acf5.jpg','2017-11-28','2017-11-28 10:58:27','2017-11-28 10:58:27'),
	(36,3,'___-2_1x.jpg','jpg','image/jpeg','e0acf070-d403-11e7-9b72-efbecd41bfe5.jpg','2017-11-28','2017-11-28 14:17:57','2017-11-28 14:17:57'),
	(37,3,'56e7c4bf90b0d.jpg','jpg','image/jpeg','58605ca0-d428-11e7-af67-09d16f275787.jpg','2017-11-28','2017-11-28 18:39:00','2017-11-28 18:39:00'),
	(38,5,'示例图片_01.jpg','jpg','image/jpeg','9beb3480-d429-11e7-81ec-f542cbd34ec9.jpg','2017-11-28','2017-11-28 18:48:03','2017-11-28 18:48:03'),
	(39,5,'示例图片_03.jpg','jpg','image/jpeg','a0161980-d429-11e7-81ec-f542cbd34ec9.jpg','2017-11-28','2017-11-28 18:48:10','2017-11-28 18:48:10'),
	(40,5,'示例图片_01.jpg','jpg','image/jpeg','a3057140-d429-11e7-81ec-f542cbd34ec9.jpg','2017-11-28','2017-11-28 18:48:15','2017-11-28 18:48:15'),
	(41,8,'unicornandreeavlad_1x.jpg','jpg','image/jpeg','23bebd20-d4c8-11e7-9db7-31e94a38010b.jpg','2017-11-29','2017-11-29 13:42:51','2017-11-29 13:42:51'),
	(42,8,'___-2_1x.jpg','jpg','image/jpeg','2be49d80-d4c8-11e7-9db7-31e94a38010b.jpg','2017-11-29','2017-11-29 13:43:05','2017-11-29 13:43:05'),
	(43,6,'c81134e3c2458f8df014b3591abf07ca.jpg','jpg','image/jpeg','4542dc00-d4dd-11e7-b561-71709d098529.jpg','2017-11-29','2017-11-29 16:14:07','2017-11-29 16:14:07'),
	(44,6,'c81134e3c2458f8df014b3591abf07ca.jpg','jpg','image/jpeg','88ba2150-d4dd-11e7-8f14-035c6d148df2.jpg','2017-11-29','2017-11-29 16:16:00','2017-11-29 16:16:00');

/*!40000 ALTER TABLE `user_image` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_info`;

CREATE TABLE `user_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `nickname` text COMMENT '昵称',
  `qq_num` text NOT NULL COMMENT 'qq号',
  `avater_id` int(11) NOT NULL DEFAULT '0' COMMENT '头像图片id',
  `sign` text COMMENT '签名',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user_info` WRITE;
/*!40000 ALTER TABLE `user_info` DISABLE KEYS */;

INSERT INTO `user_info` (`id`, `user_id`, `nickname`, `qq_num`, `avater_id`, `sign`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,2,'测试账号','892506947668',4,'测试签名信息','2017-11-27 11:53:58','2017-11-29 11:43:26',NULL),
	(2,1,'魏','731146000',7,'超难','2017-11-27 14:51:18','2017-11-28 20:08:03',NULL),
	(3,3,'QINGFul','11111222333',36,'2','2017-11-28 09:27:04','2017-11-29 16:54:19',NULL),
	(4,5,'rmba','1142006041',40,'丫丫丫丫','2017-11-28 18:45:08','2017-11-28 19:00:02',NULL),
	(5,6,'张鹏','',44,'','2017-11-28 19:11:22','2017-11-29 16:17:08',NULL),
	(6,7,'memphis','0',0,NULL,'2017-11-29 11:14:55','2017-11-29 11:14:55',NULL),
	(7,8,'num','',42,'','2017-11-29 13:41:12','2017-11-29 13:43:06',NULL);

/*!40000 ALTER TABLE `user_info` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_notice
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_notice`;

CREATE TABLE `user_notice` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `notice_id` int(11) NOT NULL DEFAULT '0' COMMENT '消息id',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0未读，1已读',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user_notice` WRITE;
/*!40000 ALTER TABLE `user_notice` DISABLE KEYS */;

INSERT INTO `user_notice` (`id`, `user_id`, `notice_id`, `status`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,1,1,NULL,'2017-11-28 20:26:36',NULL),
	(2,1,2,0,NULL,NULL,NULL);

/*!40000 ALTER TABLE `user_notice` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `role_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '角色id',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `user_role-role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table verify
# ------------------------------------------------------------

DROP TABLE IF EXISTS `verify`;

CREATE TABLE `verify` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` text,
  `value` text,
  `ip` text,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0:未使用，1:已核销',
  `remark` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `verify` WRITE;
/*!40000 ALTER TABLE `verify` DISABLE KEYS */;

INSERT INTO `verify` (`id`, `uuid`, `value`, `ip`, `status`, `remark`, `created_at`, `updated_at`)
VALUES
	(1,'d9de0774-acda-41bc-a4a2-0db38f4185b3','4','::ffff:192.168.31.13',0,NULL,'2017-11-27 10:15:00','2017-11-27 10:15:00'),
	(2,'20c0dc5d-eefa-4536-8155-5c39b5b96f23','11','::ffff:192.168.31.170',0,NULL,'2017-11-27 10:15:20','2017-11-27 10:15:20'),
	(3,'1208030d-676b-430e-9c46-56580198201c','14','::ffff:192.168.31.248',0,NULL,'2017-11-27 10:15:34','2017-11-27 10:15:34'),
	(4,'e50993fb-1d2f-4ca6-a70f-39bf1ca45524','259576','::ffff:192.168.31.13',1,'18538553590','2017-11-27 10:15:47','2017-11-27 10:16:05'),
	(5,'2bcd4f4e-555d-4093-bd82-fdc5d5569082','265932','::ffff:192.168.31.248',1,'18538069497','2017-11-27 10:15:49','2017-11-27 10:16:16'),
	(6,'c02b7b12-c780-43ce-a30b-98ef11f59646','10','::ffff:192.168.31.170',0,NULL,'2017-11-27 10:15:56','2017-11-27 10:15:56'),
	(7,'d026bf9d-7f60-4148-8c4c-b4ea4c22523c','7','::ffff:192.168.31.170',0,NULL,'2017-11-27 10:16:00','2017-11-27 10:16:00'),
	(8,'0389a031-73e0-4314-b17a-eb7b4a77b32e','7','::ffff:192.168.31.13',1,NULL,'2017-11-27 10:16:05','2017-11-27 10:16:15'),
	(9,'37fba7bb-8520-4c41-8afa-9ba0de98fe5d','071872','::ffff:192.168.31.170',1,'15738361451','2017-11-27 10:16:10','2017-11-27 10:16:33'),
	(10,'165946ee-19f2-41c5-8a5e-25cb2c749f6a','7','::ffff:192.168.31.248',1,NULL,'2017-11-27 10:16:16','2017-11-27 10:17:02'),
	(11,'41008642-fa70-40df-9e47-b5c6b8c5c067','9','::ffff:192.168.31.119',0,NULL,'2017-11-27 10:16:27','2017-11-27 10:16:27'),
	(12,'03b4a717-ad0b-43ee-bd06-4710f0066cb5','10','::ffff:192.168.31.170',0,NULL,'2017-11-27 10:16:36','2017-11-27 10:16:36'),
	(13,'ba8a9dfd-aa45-4dda-a664-200c9fb3fa43','199523','::ffff:192.168.31.170',0,'15738361451','2017-11-27 10:16:47','2017-11-27 10:16:47'),
	(14,'a1faa585-8fa5-4e25-b7a6-568fe042f541','041242','::ffff:192.168.31.170',1,'15738361451','2017-11-27 10:18:02','2017-11-27 10:18:14'),
	(15,'66ea193a-63e1-4722-a089-7d2ba53633cb','13','::ffff:192.168.31.170',1,NULL,'2017-11-27 10:18:14','2017-11-27 10:18:30'),
	(16,'9191ce99-e213-49e1-8cf1-3562354145bf','14','::ffff:192.168.31.119',0,NULL,'2017-11-27 10:22:33','2017-11-27 10:22:33'),
	(17,'2e995cfb-4f3d-41cb-bf9b-286dcd3698c5','9','::ffff:192.168.31.170',0,NULL,'2017-11-27 10:22:56','2017-11-27 10:22:56'),
	(18,'42f0d12d-bd3b-4fef-b8f0-1b856b05ff34','13','::ffff:192.168.31.170',0,NULL,'2017-11-27 10:23:25','2017-11-27 10:23:25'),
	(19,'062f72ce-641e-4335-97db-26b204adafea','11','::ffff:192.168.31.170',0,NULL,'2017-11-27 10:23:36','2017-11-27 10:23:36'),
	(20,'a39bb158-510f-468e-bff3-18e840365da0','18','::ffff:192.168.31.170',1,NULL,'2017-11-27 10:23:50','2017-11-27 10:23:57'),
	(21,'0af2d2b2-05c4-49d9-9460-bf74d5a57ef3','6','::ffff:192.168.31.119',0,NULL,'2017-11-27 10:27:07','2017-11-27 10:27:07'),
	(22,'427d4a64-49dd-4a14-b2f1-e86014d4af14','10','::ffff:192.168.31.119',0,NULL,'2017-11-27 10:27:16','2017-11-27 10:27:16'),
	(23,'5ce5c022-7d34-4330-b9c1-c19f91cfb6ee','10','::ffff:192.168.31.170',1,NULL,'2017-11-27 10:28:56','2017-11-27 10:29:04'),
	(24,'0272e7b3-938b-46f2-a087-7787cfbd8ffd','4','::ffff:192.168.31.119',0,NULL,'2017-11-27 10:47:25','2017-11-27 10:47:25'),
	(25,'a08c3fd9-5cb3-4795-99e6-8993af6009f1','15','::ffff:192.168.31.119',0,NULL,'2017-11-27 10:47:28','2017-11-27 10:47:28'),
	(26,'39c52b11-f500-488a-8f0e-983e1a5db5dc','10','::ffff:192.168.31.119',1,NULL,'2017-11-27 10:47:37','2017-11-27 10:47:52'),
	(27,'50f7b8e5-3b0b-4c3a-9590-069951b2ecb1','12','::ffff:192.168.31.155',1,NULL,'2017-11-27 11:08:04','2017-11-27 11:08:11'),
	(28,'330c4030-80ea-45f0-a86a-0033524b1084','6','::ffff:192.168.31.119',1,NULL,'2017-11-27 11:08:05','2017-11-27 11:08:08'),
	(29,'4cb2b7cb-60df-4572-8bef-79de0c48fa13','12','::ffff:192.168.31.119',0,NULL,'2017-11-27 11:27:32','2017-11-27 11:27:32'),
	(30,'0ece2928-db8e-4f12-8077-405db7b10011','18','::ffff:192.168.31.170',1,NULL,'2017-11-27 11:35:21','2017-11-27 11:35:31'),
	(31,'b955e6df-bd04-4fa2-9a8c-02c25c1c7e96','10','::ffff:192.168.31.155',0,NULL,'2017-11-27 13:50:03','2017-11-27 13:50:03'),
	(32,'513b0209-d0f3-4e68-a435-65200c111262','136750','::ffff:192.168.31.155',1,'17638188752','2017-11-27 13:50:12','2017-11-27 13:50:36'),
	(33,'2988f947-b46b-4325-942a-a51ea1fadf8f','6','::ffff:192.168.31.155',1,NULL,'2017-11-27 13:50:36','2017-11-27 13:50:48'),
	(34,'4ca29db6-e3ec-4175-ab22-31cff82bf883','8','::ffff:192.168.31.248',1,NULL,'2017-11-27 14:15:14','2017-11-27 14:15:26'),
	(35,'4607d370-3b45-4292-ad9c-6efcda6233b1','8','::ffff:192.168.31.248',1,NULL,'2017-11-27 14:15:57','2017-11-27 14:15:58'),
	(36,'01c6a936-868b-43eb-8d1e-a56fcbae057f','10','::ffff:192.168.31.248',1,NULL,'2017-11-27 14:16:04','2017-11-27 14:16:07'),
	(37,'6a7350f3-b324-4999-9f2b-d4529a10f6c9','7','::ffff:192.168.31.248',0,NULL,'2017-11-27 14:17:20','2017-11-27 14:17:20'),
	(38,'996509c8-b461-4572-8b70-e4b13802a988','6','::ffff:192.168.31.248',0,NULL,'2017-11-27 14:17:37','2017-11-27 14:17:37'),
	(39,'17ccb83f-6720-4084-816c-39eeb7836f60','11','::ffff:192.168.31.248',0,NULL,'2017-11-27 14:17:38','2017-11-27 14:17:38'),
	(40,'ff8babe1-ba80-443f-ab58-419b8732179c','16','::ffff:192.168.31.248',0,NULL,'2017-11-27 14:17:39','2017-11-27 14:17:39'),
	(41,'54f396ef-ed6f-4929-824e-fd5039d334be','17','::ffff:192.168.31.248',0,NULL,'2017-11-27 14:17:39','2017-11-27 14:17:39'),
	(42,'f4879ab0-7073-45f5-a5a2-230aeda7b337','9','::ffff:192.168.31.248',0,NULL,'2017-11-27 14:17:39','2017-11-27 14:17:39'),
	(43,'6cced086-ca9b-48ce-9eee-fd02eac1f604','4','::ffff:192.168.31.248',1,NULL,'2017-11-27 14:17:42','2017-11-27 14:17:55'),
	(44,'09ecebb9-f488-4118-95fc-3f55becf7901','16','::ffff:192.168.31.119',1,NULL,'2017-11-27 14:26:46','2017-11-27 14:26:58'),
	(45,'f650318a-157d-459f-bc91-a0d4a99848fc','5','::ffff:192.168.31.248',1,NULL,'2017-11-27 14:33:23','2017-11-27 14:38:12'),
	(46,'80ffd573-2925-4cd2-ba88-959d4043246d','10','::ffff:192.168.31.248',0,NULL,'2017-11-27 14:33:28','2017-11-27 14:33:28'),
	(47,'fb547713-1edb-4b72-a8e6-cd2c3dcbc9fe','8','::ffff:192.168.31.248',0,NULL,'2017-11-27 14:40:36','2017-11-27 14:40:36'),
	(48,'b62cd71b-2db9-4f50-a381-852cd14a9dc1','10','::ffff:192.168.31.248',0,NULL,'2017-11-27 14:40:37','2017-11-27 14:40:37'),
	(49,'0be2cc03-42bf-4f58-876d-de184fdb39c5','13','::ffff:192.168.31.170',0,NULL,'2017-11-27 14:50:39','2017-11-27 14:50:39'),
	(50,'3101486d-f434-4477-af42-d507af2ab5b1','9','::ffff:192.168.31.170',1,NULL,'2017-11-27 14:50:42','2017-11-27 14:50:53'),
	(51,'4e58f219-e5ec-462c-b30b-d93e0dae61c6','17','::ffff:192.168.31.248',1,NULL,'2017-11-27 14:57:57','2017-11-27 14:58:09'),
	(52,'5adaa59c-0d58-4236-8a24-76c6dcba02e5','8','::ffff:192.168.31.170',0,NULL,'2017-11-27 15:12:53','2017-11-27 15:12:53'),
	(53,'6377375b-a324-4c6b-8fac-d60026e00168','16','::ffff:192.168.31.170',0,NULL,'2017-11-27 15:13:01','2017-11-27 15:13:01'),
	(54,'b4ec1455-ca7c-4c93-b3ac-fe68ee183d50','13','::ffff:192.168.31.170',0,NULL,'2017-11-27 15:13:05','2017-11-27 15:13:05'),
	(55,'ab169220-bae2-4479-af49-19f0f4dc2d5b','10','::ffff:192.168.31.170',0,NULL,'2017-11-27 15:21:46','2017-11-27 15:21:46'),
	(56,'24750591-a68d-46f8-bf4a-4f2b81a38fc1','4','::ffff:192.168.31.170',0,NULL,'2017-11-27 15:21:49','2017-11-27 15:21:49'),
	(57,'e5aacaec-02fb-425a-98ea-51b5a29dfbd5','5','::ffff:192.168.31.170',0,NULL,'2017-11-27 15:21:55','2017-11-27 15:21:55'),
	(58,'00c3a3da-3c10-4ea3-aea7-a45e657f6535','10','::ffff:192.168.31.170',0,NULL,'2017-11-27 15:22:00','2017-11-27 15:22:00'),
	(59,'1589683a-4f90-4897-b323-97e87c13365b','9','::ffff:192.168.31.170',1,NULL,'2017-11-27 15:22:51','2017-11-27 15:23:01'),
	(60,'961a8b2d-8511-4b0b-8313-aaa3ce88b1cb','9','::ffff:192.168.31.170',0,NULL,'2017-11-27 15:24:01','2017-11-27 15:24:01'),
	(61,'47f760f3-9e20-4207-bd76-74ff8223f902','9','::ffff:192.168.31.170',0,NULL,'2017-11-27 15:25:16','2017-11-27 15:25:16'),
	(62,'a474fcab-ca1f-4013-9a41-b524ef90db16','4','::ffff:192.168.31.170',0,NULL,'2017-11-27 15:29:23','2017-11-27 15:29:23'),
	(63,'a315dca0-0fe8-49a3-b26e-7a8363eac8fa','775901','::ffff:192.168.31.170',0,'15738361451','2017-11-27 15:29:32','2017-11-27 15:29:32'),
	(64,'cf492e07-0772-4dc2-976d-0efa9e8c2c22','123033','::ffff:192.168.31.170',0,'15738361451','2017-11-27 15:31:38','2017-11-27 15:31:38'),
	(65,'34dc8431-454b-4374-bb27-4924a9aeda07','14','::ffff:192.168.31.170',1,NULL,'2017-11-27 15:32:43','2017-11-27 15:32:53'),
	(66,'79c10b09-eacc-4ead-ac5f-80726b97549a','16','::ffff:192.168.31.170',0,NULL,'2017-11-27 15:43:31','2017-11-27 15:43:31'),
	(67,'fddceb84-e33d-45d6-b62e-9863aee5f069','364347','::ffff:192.168.31.170',0,'15738361452','2017-11-27 15:43:44','2017-11-27 15:43:44'),
	(68,'ae814132-2c07-47fd-a765-1f9ac79a7f35','830044','::ffff:192.168.31.170',0,'15738361452','2017-11-27 15:45:05','2017-11-27 15:45:05'),
	(69,'8be62f9a-2049-49df-b222-4c47b10967d6','8','::ffff:192.168.31.170',0,NULL,'2017-11-27 15:45:12','2017-11-27 15:45:12'),
	(70,'cf7e7785-a67f-4901-877e-ccca0e68b44f','430033','::ffff:192.168.31.170',1,'15738361451','2017-11-27 15:46:32','2017-11-27 15:46:49'),
	(71,'9fece93b-507c-4be0-9320-1f08ac8771e7','10','::ffff:192.168.31.170',1,NULL,'2017-11-27 15:46:49','2017-11-27 15:46:59'),
	(72,'074baa91-56e9-4d26-96b8-f26d45818d35','11','::ffff:192.168.31.248',1,NULL,'2017-11-27 16:05:38','2017-11-27 16:05:41'),
	(73,'ff165e31-6fdf-4f6b-987d-b45cd9173c4d','14','::ffff:192.168.31.155',0,NULL,'2017-11-27 17:50:13','2017-11-27 17:50:13'),
	(74,'252e4ef6-54ec-410b-8783-ede81723f298','16','::ffff:192.168.31.155',0,NULL,'2017-11-27 17:50:16','2017-11-27 17:50:16'),
	(75,'6ec4b510-354c-4ed3-996d-9669e8e4889d','5','::ffff:192.168.31.155',0,NULL,'2017-11-27 17:50:18','2017-11-27 17:50:18'),
	(76,'95f111f2-e995-499d-ab6f-b63e443c5980','5','::ffff:192.168.31.155',0,NULL,'2017-11-27 17:50:23','2017-11-27 17:50:23'),
	(77,'4b8bccca-a8e1-4129-90bb-4e39d1f83e46','10','::ffff:192.168.31.155',0,NULL,'2017-11-27 17:50:24','2017-11-27 17:50:24'),
	(78,'651ecc33-6bd9-4c2f-9ef8-07c4968542bc','11','::ffff:192.168.31.155',0,NULL,'2017-11-27 17:50:25','2017-11-27 17:50:25'),
	(79,'d959a857-4046-44f0-b904-0e69eb5f5914','11','::ffff:192.168.31.170',1,NULL,'2017-11-27 18:21:47','2017-11-27 18:22:11'),
	(80,'3ecd69eb-0c24-4748-ad8b-77b1197f5480','10','::ffff:192.168.31.170',1,NULL,'2017-11-27 18:23:01','2017-11-27 18:23:10'),
	(81,'1e0b3f89-7c0e-46cb-a43b-03200e739f6d','16','::ffff:192.168.31.170',1,NULL,'2017-11-27 18:38:21','2017-11-27 18:38:29'),
	(82,'b54dbb35-c7f3-4f83-8c60-ff2468ab725e','7','::ffff:192.168.31.170',1,NULL,'2017-11-27 19:28:01','2017-11-27 19:28:09'),
	(83,'144ae126-a0ac-43db-81d1-1077854cfd89','11','::ffff:192.168.31.170',0,NULL,'2017-11-27 19:28:57','2017-11-27 19:28:57'),
	(84,'08ed0af6-c21b-4afb-a304-07db149a1b32','10','::ffff:192.168.31.170',0,NULL,'2017-11-27 19:29:33','2017-11-27 19:29:33'),
	(85,'61d34cdb-ca41-4df9-9297-fdd9d8f5d375','4','::ffff:192.168.31.170',0,NULL,'2017-11-27 19:29:38','2017-11-27 19:29:38'),
	(86,'c5e8c8a6-af8b-4a4a-8183-9f29140f7209','13','::ffff:192.168.31.170',1,NULL,'2017-11-27 19:29:44','2017-11-27 19:29:52'),
	(87,'7a1a9db3-b299-411c-94a7-644a1ff70d7b','11','::ffff:192.168.31.170',0,NULL,'2017-11-27 19:33:03','2017-11-27 19:33:03'),
	(88,'051ef47c-393d-4c4e-96bc-994793ae51c7','9','::ffff:192.168.31.170',0,NULL,'2017-11-27 19:34:20','2017-11-27 19:34:20'),
	(89,'f38172a1-e43b-4cd1-a83e-fdd66d0eb56d','10','::ffff:192.168.31.170',0,NULL,'2017-11-27 19:34:25','2017-11-27 19:34:25'),
	(90,'5c65ab2a-0f58-4c73-b730-e533fe769ffc','11','::ffff:192.168.31.170',0,NULL,'2017-11-27 19:34:27','2017-11-27 19:34:27'),
	(91,'9f0e26e7-e44c-4326-ba5e-089a2dafaade','16','::ffff:192.168.31.170',0,NULL,'2017-11-27 19:36:06','2017-11-27 19:36:06'),
	(92,'cad31531-9934-445f-9cc1-845082298a5d','18','::ffff:192.168.31.170',0,NULL,'2017-11-27 19:36:10','2017-11-27 19:36:10'),
	(93,'aa113e4e-d5ad-4585-8f39-5aebacab7c64','9','::ffff:192.168.31.170',0,NULL,'2017-11-27 19:36:13','2017-11-27 19:36:13'),
	(94,'8b01196a-089a-4f5b-9eb0-07c216336817','12','::ffff:192.168.31.170',0,NULL,'2017-11-27 19:36:17','2017-11-27 19:36:17'),
	(95,'63404719-dcce-40fa-880f-08caa13dacac','10','::ffff:192.168.31.170',0,NULL,'2017-11-27 19:36:39','2017-11-27 19:36:39'),
	(96,'63f9c0a8-b633-4d4d-af85-419103f044ef','541446','::ffff:192.168.31.170',1,'15738361451','2017-11-27 19:38:59','2017-11-27 19:39:15'),
	(97,'0b8d35e7-df7b-44fe-ae46-5953cd813da5','12','::ffff:192.168.31.170',1,NULL,'2017-11-27 19:39:51','2017-11-27 19:40:01'),
	(98,'ac95aba0-6119-4740-bd67-0a302d9621d8','11','::ffff:192.168.31.155',1,NULL,'2017-11-27 19:40:42','2017-11-27 19:40:51'),
	(99,'04ede574-e7df-4511-94e0-a6b9c9f9efe2','13','::ffff:192.168.31.170',0,NULL,'2017-11-28 10:01:09','2017-11-28 10:01:09'),
	(100,'76510fa4-18e9-4170-a5d6-9dd02d2587cf','6','::ffff:192.168.31.170',1,NULL,'2017-11-28 10:01:14','2017-11-28 10:01:27'),
	(101,'27ec7c2c-9e2d-4eb7-ab7a-9affb75ebae8','6','::ffff:192.168.31.119',0,NULL,'2017-11-28 10:01:18','2017-11-28 10:01:18'),
	(102,'510310f2-2897-4f82-8c1f-3732b675e483','14','::ffff:192.168.31.119',0,NULL,'2017-11-28 10:01:29','2017-11-28 10:01:29'),
	(103,'65ddeebb-8bf0-4e11-9cbc-f223b24b2fc2','10','::ffff:192.168.31.119',0,NULL,'2017-11-28 10:01:30','2017-11-28 10:01:30'),
	(104,'f3f95985-738f-4b4d-9f5f-a0c977d93417','10','::ffff:192.168.31.119',0,NULL,'2017-11-28 10:01:35','2017-11-28 10:01:35'),
	(105,'197ee1fe-eaf4-4258-867b-98287749c71f','6','::ffff:192.168.31.119',0,NULL,'2017-11-28 10:02:16','2017-11-28 10:02:16'),
	(106,'4d2d3468-9ada-4995-8fbf-2f936aac9c27','9','::ffff:192.168.31.119',0,NULL,'2017-11-28 10:02:21','2017-11-28 10:02:21'),
	(107,'8560e0b7-9899-4fd3-9d4a-e6b4e9acc182','13','::ffff:192.168.31.119',0,NULL,'2017-11-28 10:15:25','2017-11-28 10:15:25'),
	(108,'0256c5e3-769e-4943-9132-fb75fbd4d413','11','::ffff:192.168.31.101',0,NULL,'2017-11-28 10:16:32','2017-11-28 10:16:32'),
	(109,'b6cd7090-065b-41aa-a9db-769d37670499','6','::ffff:192.168.31.101',0,NULL,'2017-11-28 10:29:02','2017-11-28 10:29:02'),
	(110,'b2c332d6-58b4-4cee-b2c4-3f159ef596fe','8','::ffff:192.168.31.101',0,NULL,'2017-11-28 10:39:52','2017-11-28 10:39:52'),
	(111,'97a91f11-d046-470c-a71b-8eeb14800e42','11','::ffff:192.168.31.101',0,NULL,'2017-11-28 10:40:26','2017-11-28 10:40:26'),
	(112,'03f82f27-3fdb-49b2-8ca5-2e6798ed032f','8','::ffff:192.168.31.101',1,NULL,'2017-11-28 10:41:53','2017-11-28 11:40:51'),
	(113,'f0a961f1-4e61-413b-a208-fc43fe517b49','9','::ffff:192.168.31.119',1,NULL,'2017-11-28 10:52:58','2017-11-28 10:53:14'),
	(114,'bc97a74e-bfbc-462e-bdea-dae5bcb068f7','11','::ffff:192.168.31.119',0,NULL,'2017-11-28 13:57:47','2017-11-28 13:57:47'),
	(115,'c05148dd-ae1f-4d43-8d0d-890561bcc66f','7','::ffff:192.168.31.119',0,NULL,'2017-11-28 13:59:02','2017-11-28 13:59:02'),
	(116,'6fe7953c-7bc0-4592-b1ab-2adc37543ea0','5','::ffff:192.168.31.119',1,NULL,'2017-11-28 13:59:34','2017-11-28 13:59:56'),
	(117,'7d9b219f-d1c4-459d-9082-d1588b55086f','11','::ffff:192.168.31.119',0,NULL,'2017-11-28 14:01:34','2017-11-28 14:01:34'),
	(118,'696ab527-4966-406a-8724-07d2a3a2b603','7','::ffff:192.168.31.119',0,NULL,'2017-11-28 14:04:02','2017-11-28 14:04:02'),
	(119,'6a7e6b09-ab7e-4094-9d55-255340aef5a7','9','::ffff:192.168.31.170',0,NULL,'2017-11-28 14:05:05','2017-11-28 14:05:05'),
	(120,'6b9e95d0-ae94-4f4a-8af3-8f2d201706b2','6','::ffff:192.168.31.119',0,NULL,'2017-11-28 14:05:17','2017-11-28 14:05:17'),
	(121,'64a93a85-fef6-4c74-b215-bab59afd5521','15','::ffff:192.168.31.119',1,NULL,'2017-11-28 14:05:22','2017-11-28 14:05:28'),
	(122,'8bc47c59-65d9-4132-b756-ca525fa3cda7','12','::ffff:192.168.31.170',0,NULL,'2017-11-28 14:05:24','2017-11-28 14:05:24'),
	(123,'259503e0-f4dd-41e7-8953-513dceaffd6b','10','::ffff:192.168.31.170',0,NULL,'2017-11-28 14:05:30','2017-11-28 14:05:30'),
	(124,'dcc0a9cd-3fdd-4dff-b193-4a1904033d4a','7','::ffff:192.168.31.170',1,NULL,'2017-11-28 14:06:50','2017-11-28 14:07:00'),
	(125,'f15c9c53-ee11-421b-a958-2e1d9e9c3c65','12','::ffff:192.168.31.119',0,NULL,'2017-11-28 14:08:02','2017-11-28 14:08:02'),
	(126,'09533165-7cd3-4846-9af5-ff4a46201745','12','::ffff:192.168.31.119',0,NULL,'2017-11-28 14:08:14','2017-11-28 14:08:14'),
	(127,'8bff32b2-26eb-4f58-a2cc-71cef18d941a','14','::ffff:192.168.31.119',0,NULL,'2017-11-28 14:08:19','2017-11-28 14:08:19'),
	(128,'5a198423-9430-4eb4-a5a8-407bbb69f9f9','10','::ffff:192.168.31.119',1,NULL,'2017-11-28 14:08:21','2017-11-28 14:08:27'),
	(129,'64f43743-ecdf-452a-80e2-1062581379ae','12','::ffff:192.168.31.13',1,NULL,'2017-11-28 14:22:40','2017-11-28 14:22:46'),
	(130,'b5f16751-40cb-42df-ae34-e1881cea2bd3','9','::ffff:192.168.31.13',1,NULL,'2017-11-28 15:49:52','2017-11-28 15:50:01'),
	(131,'d45551e8-f0d4-446f-9f33-b739b393c723','12','::ffff:192.168.31.13',1,NULL,'2017-11-28 15:59:10','2017-11-28 15:59:27'),
	(132,'9cdbc231-8260-446a-97bf-b3913c1d5784','5','::ffff:192.168.31.124',0,NULL,'2017-11-28 18:44:21','2017-11-28 18:44:21'),
	(133,'be29164b-e73b-4057-8048-dd229fd37218','15','::ffff:192.168.31.119',0,NULL,'2017-11-28 18:44:31','2017-11-28 18:44:31'),
	(134,'f053a194-c5bb-4dc4-801b-c7bf256d2ac2','3','::ffff:192.168.31.119',1,NULL,'2017-11-28 18:44:35','2017-11-28 20:07:01'),
	(135,'a9e028c7-9cb0-4f0a-9b22-9cdd7dfddcb6','630920','::ffff:192.168.31.124',1,'13253522080','2017-11-28 18:44:47','2017-11-28 18:45:08'),
	(136,'40c7123d-5878-49a2-825b-64e0f2f6c6e9','10','::ffff:192.168.31.124',1,NULL,'2017-11-28 18:45:08','2017-11-28 18:45:22'),
	(137,'c39cd084-3669-4b78-8a1b-30c3f93cf9e5','9','::ffff:192.168.31.13',1,NULL,'2017-11-28 18:54:01','2017-11-28 18:58:35'),
	(138,'004aa8a7-006f-46de-ae45-ae3fca283a72','18','::ffff:192.168.31.124',1,NULL,'2017-11-28 19:01:10','2017-11-28 19:01:24'),
	(139,'1564fcdc-69f7-4d75-963e-45e75cf0b928','7','::ffff:192.168.31.193',1,NULL,'2017-11-28 19:10:02','2017-11-28 19:11:38'),
	(140,'3a4e8bcf-67f5-4216-8666-c5e32578e177','816920','::ffff:192.168.31.193',1,'18338767481','2017-11-28 19:10:54','2017-11-28 19:11:22'),
	(141,'d81b43a7-5929-4974-9ee6-ac649e250046','14','::ffff:192.168.31.193',1,NULL,'2017-11-28 19:15:51','2017-11-28 19:16:10'),
	(142,'437fcac4-afb1-4cc4-986e-82dbfdaf120e','9','::ffff:192.168.31.13',1,NULL,'2017-11-28 19:21:56','2017-11-28 19:22:06'),
	(143,'78fed248-b9ef-4533-bd66-ac1abe2b98f6','12','::ffff:192.168.31.119',1,NULL,'2017-11-28 19:36:01','2017-11-28 19:40:56'),
	(144,'160be614-e173-4857-ac39-eb54d88f1af0','13','::ffff:192.168.31.106',1,NULL,'2017-11-28 19:43:55','2017-11-28 19:44:07'),
	(145,'1505975d-4280-43b9-a7bf-e385854754cf','11','::ffff:192.168.31.170',0,NULL,'2017-11-28 19:59:49','2017-11-28 19:59:49'),
	(146,'3b4dfc97-7477-456b-b631-fe8eea516d05','655948','::ffff:192.168.31.170',1,'15738361451','2017-11-28 20:00:19','2017-11-28 20:00:31'),
	(147,'4b714aba-a1e5-47e2-a420-514b428837d0','9','::ffff:192.168.31.170',1,NULL,'2017-11-28 20:00:31','2017-11-28 20:00:47'),
	(148,'0210c50a-9e29-4b45-82be-91755c8c5a84','7','::ffff:192.168.31.170',0,NULL,'2017-11-28 20:10:57','2017-11-28 20:10:57'),
	(149,'22ec876b-e041-4539-89d1-b14cda4e7008','7','::ffff:192.168.31.170',0,NULL,'2017-11-28 20:11:01','2017-11-28 20:11:01'),
	(150,'508329d1-5873-4112-8505-bc88ec9b6a45','10','::ffff:192.168.31.170',0,NULL,'2017-11-28 20:11:04','2017-11-28 20:11:04'),
	(151,'df927dfc-3da7-47fa-ac1d-4b2013c2830b','8','::ffff:192.168.31.170',0,NULL,'2017-11-28 20:11:10','2017-11-28 20:11:10'),
	(152,'3b7bd13c-a154-46c9-87a5-8c5b08632753','9','::ffff:192.168.31.170',1,NULL,'2017-11-28 20:11:12','2017-11-28 20:11:19'),
	(153,'3eaa10f1-bb62-4709-8d0a-b79fcc917597','6','::ffff:192.168.31.170',1,NULL,'2017-11-28 22:41:11','2017-11-28 22:41:19'),
	(154,'d4afb00f-0808-434b-8739-eacc4405c119','11','::ffff:192.168.31.124',1,NULL,'2017-11-29 09:40:06','2017-11-29 09:40:19'),
	(155,'782e5e73-7058-45dc-832b-dd171d039bcf','12','::ffff:192.168.31.194',0,NULL,'2017-11-29 11:11:39','2017-11-29 11:11:39'),
	(156,'947b2035-c564-4f76-ba5e-99a93d271ad1','376203','::ffff:192.168.31.194',1,'18790287977','2017-11-29 11:14:36','2017-11-29 11:14:55'),
	(157,'ca247d9b-b84e-4653-aeea-f214e4b5a438','15','::ffff:192.168.31.194',1,NULL,'2017-11-29 11:14:56','2017-11-29 11:15:13'),
	(158,'f0cd29b6-29c6-4c41-b6d6-4871b4e623fb','13','::ffff:192.168.31.194',1,NULL,'2017-11-29 11:15:33','2017-11-29 11:15:38'),
	(159,'27006c82-51ea-47b7-a03d-95b5bd5a4c72','8','::ffff:192.168.31.13',1,NULL,'2017-11-29 11:19:56','2017-11-29 11:20:05'),
	(160,'a80180c8-fc7f-4880-be39-35469c1fbebd','4','::ffff:192.168.31.13',1,NULL,'2017-11-29 11:36:33','2017-11-29 11:36:46'),
	(161,'4fbb493d-99d0-43e2-a976-0036bae2361f','14','::ffff:192.168.31.193',1,NULL,'2017-11-29 11:49:38','2017-11-29 11:49:58'),
	(162,'11cdbdcf-eda6-4d5a-ba81-9fe66d69bd0b','13','::ffff:192.168.31.124',1,NULL,'2017-11-29 11:51:10','2017-11-29 11:51:26'),
	(163,'78f506a3-3c5c-43b3-8cb6-eb79c9ef6fbb','15','::ffff:192.168.31.248',0,NULL,'2017-11-29 13:37:54','2017-11-29 13:37:54'),
	(164,'c26c2c05-5e5d-41d6-be98-6c2aae3be9f4','12','::ffff:192.168.31.248',1,NULL,'2017-11-29 13:37:54','2017-11-29 13:38:06'),
	(165,'7270d1c2-f104-4858-bfe1-3a9e9467ca85','7','::ffff:192.168.31.13',1,NULL,'2017-11-29 13:39:00','2017-11-29 13:39:11'),
	(166,'10e9e36e-7df6-44f1-8cba-727393ead202','8','::ffff:192.168.31.119',0,NULL,'2017-11-29 13:40:18','2017-11-29 13:40:18'),
	(167,'33376b38-5178-4573-9bc2-578562764857','14','::ffff:192.168.31.248',1,NULL,'2017-11-29 13:40:28','2017-11-29 14:45:45'),
	(168,'02994e27-6f86-43db-bc89-a032c5de800d','9','::ffff:192.168.31.101',1,NULL,'2017-11-29 13:40:28','2017-11-29 13:41:10'),
	(169,'c5a09ca8-db9f-4c3e-9333-6ecb5e88c9d4','284638','::ffff:192.168.31.119',1,'17516209689','2017-11-29 13:40:33','2017-11-29 13:41:12'),
	(170,'07120625-7244-41c3-beed-26e61673c866','8','::ffff:192.168.31.119',1,NULL,'2017-11-29 13:41:12','2017-11-29 13:41:33'),
	(171,'a79e5332-4bae-4d2a-a3a8-0829cf4b92cc','12','::ffff:192.168.31.155',1,NULL,'2017-11-29 14:20:03','2017-11-29 14:20:22'),
	(172,'90bf95c8-9ee8-4e63-ad59-1728164f7f38','13','::ffff:192.168.31.119',0,NULL,'2017-11-29 14:56:23','2017-11-29 14:56:23'),
	(173,'b92003d7-a941-4a72-b1ec-38d535829de8','5','::ffff:192.168.31.119',1,NULL,'2017-11-29 14:57:15','2017-11-29 14:57:27'),
	(174,'c3d82117-3cc1-4b80-9295-f098c9929b99','12','::ffff:192.168.31.119',1,NULL,'2017-11-29 15:24:27','2017-11-29 15:25:00'),
	(175,'a0fc6501-558b-40ea-9945-551e91eccd28','11','::ffff:192.168.31.248',1,NULL,'2017-11-29 16:09:43','2017-11-29 16:09:58'),
	(176,'5b2f0bc2-61bf-43b7-9959-247481c174c8','7','::ffff:192.168.31.13',1,NULL,'2017-11-29 16:46:19','2017-11-29 16:46:24'),
	(177,'931a28e1-a1fd-4e06-a864-b254138bbee9','10','::ffff:192.168.31.194',1,NULL,'2017-11-29 16:51:56','2017-11-29 16:52:06'),
	(178,'66ee4532-f611-42e1-89db-f4d56f12040f','14','::ffff:192.168.31.13',1,NULL,'2017-11-29 17:14:38','2017-11-29 17:14:49'),
	(179,'1872540b-cca3-458c-a7ce-295515cda22e','11','::ffff:192.168.31.194',1,NULL,'2017-11-29 17:34:26','2017-11-29 17:34:52'),
	(180,'15f68fd5-77f0-49e4-8a57-15fc4a14092a','7','::ffff:192.168.31.194',1,NULL,'2017-11-29 17:56:03','2017-11-29 17:56:12'),
	(181,'49798a0c-fbd2-4857-9026-bf2a4df5da5c','16','::ffff:192.168.31.13',1,NULL,'2017-11-29 17:58:50','2017-11-29 17:59:03'),
	(182,'adf4a94e-f237-43b4-97a3-045641894198','13','::ffff:192.168.31.194',1,NULL,'2017-11-29 18:01:51','2017-11-29 18:01:57'),
	(183,'8df44dd9-39ea-43c1-81ec-c1cf013a17f1','9','::ffff:192.168.31.13',1,NULL,'2017-11-29 18:03:51','2017-11-29 18:04:01');

/*!40000 ALTER TABLE `verify` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
